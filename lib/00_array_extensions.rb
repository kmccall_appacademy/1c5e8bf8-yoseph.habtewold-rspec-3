require "byebug"

class Array
  def sum
    self.inject(0,:+)
  end

  def square!
    self.map!{|el| el*el}
  end

  def square
    self.map{|el| el*el}
  end

  def my_uniq
    uniq = []
    self.each {|el| uniq << el if !uniq.include?(el)}
    uniq
  end

  def two_sum
    result = []
    self.each_with_index do |el, idx|
      (idx+1...self.length).each do |i|
        result << [idx,i]if el + self[i] == 0
      end
    end
    result
  end

  def median
    return nil if self.empty?
    sorted = self.sort

    sorted.length.even? ? even_median(sorted) : odd_median(sorted)
  end

  def even_median(arr)
    (arr[(length/2) - 1] + arr[length/2]) / 2.0
  end

  def odd_median(arr)
    arr[length/2]
  end

  def my_transpose
    row_size = self[0].length
    col_size = self.length
    result = Array.new(row_size){ Array.new(col_size) }

    self.each_with_index do |col, col_idx|
      col.each_with_index do |row, row_idx|
        result[row_idx][col_idx] = self[col_idx][row_idx]
      end
    end
    result
  end
end
